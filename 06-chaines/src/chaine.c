#include "chaine.h"
#include "stdlib.h"
#include "string.h"

/*  Inversion de chaine
    Écrire la fonction inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
    char* inverser(char *str)
    bonjour →  ruojnob*/
char *inverser(char *str)
{
    int size = strlen(str);
    char *tmp = malloc(size);
    int i;
    for (i = size - 1; i >= 0; i--)
    {
        tmp[size - i - 1] = str[i];
    }
    tmp[size] = '\0';
    return tmp;
}
/*  Palindrome
    Écrire une méthode Palindrome qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
    int palindrome(char *str)
        SOS →  1
        Bonjour →  0
        radar →  1*/
int palindrome(char *str)
{
    int r = 0;
    if (str != NULL)
    {
        char *inv = inverser(str);
        r = strcmp(str, inv) == 0;
        free(inv);
    }
    return r;
}
