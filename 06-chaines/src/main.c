#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "chaine.h"
#include <ctype.h>
#include <math.h>

/* pour utiliser M_PI avec le compilateur de microsoft*/
/* #define _USE_MATH_DEFINES */

int main(void)
{

    /* Chaine de caracteres constante */
    char *str1 = "Hello world";
    printf("%s\n", str1);
    /*str1[0]='h';*/

    /* Chaine de caractères */
    char str[20] = "hello world"; /* hello world\0 */
    printf("%s\n", str);
    str[0] = 'H';
    printf("%s\n", str);

    char vide[] = "";             /* \0 */
    printf("%d\n", sizeof(vide)); /* 1 octet pour le terminateur \0 */

    /* Longueur d'une chaine */
    printf("%d\n", strlen(str)); /* 11 caractères */

    /* Concaténation */
    strcat(str, "!!!!");
    printf("%s\n", str);

    strncat(str, "------------", 2); /* on concaténe 2 -- */
    printf("%s\n", str);

    /* Comparaison */
    char str2[] = "Hello world";
    char str3[] = "Hello wo";
    char str4[] = "Bonjour";
    char str5[] = "Aonjour";
    printf("%d\n", strcmp(str1, str2)); /* 0 str1=str2*/
    printf("%d\n", strcmp(str2, str3)); /* 1 str2 < str3 nombre de caractère*/
    printf("%d\n", strcmp(str3, str2)); /* -1 str3 < str2 nombre de caractère*/
    printf("%d\n", strcmp(str5, str4)); /* -1 A< B*/
    printf("%d\n", strcmp(str4, str5)); /*  1 -> B< A*/

    char str6[] = "Hello__world";
    printf("%d\n", strncmp(str1, str6, 5)); /* compare le 5 premier caractères */

    /* Copier une chaine */
    char str8[20];
    strcpy(str8, str1);
    printf("%s\n", str8);

    strncpy(str8, str2, 5);
    str8[5] = '\0';
    printf("%s\n", str8);

    /* Rechercher un caractère dans une chaine de caractère */
    char *pchr = strchr(str2, 'o');
    printf("%p %p %d\n", str2, pchr, pchr - str2); /* 4 */

    char *pchr2 = strrchr(str2, 'o');
    printf("%p %p %d\n", str2, pchr2, pchr2 - str2); /* 7 */

    char *pchr3 = strchr(str2, 'z'); /* si le caractère n'est pas trouvé -> NULL*/
    printf("%p\n", pchr3);

    /* Rechercher une sous-chaine de caractère dans une chaine de caractère */
    char *pChr4 = strstr(str2, "lo");
    printf("%p %p %d\n", str2, pChr4, pChr4 - str2);

    /* Découper une chaine en fonction d'un ou de plusieurs séparteur */
    char str7[] = "aze\nrtyu\tipqs,dfg";
    char *separateur = "\t\n ,";
    char *strToken = strtok(str7, separateur);
    while (strToken != NULL)
    {
        printf("%s\n", strToken);
        strToken = strtok(NULL, separateur);
    }

    /* Atelier: inverser une chaine */
    char strSc[100];
    scanf("%s", strSc);
    char *inv = inverser(strSc);
    printf("%s\n", inv);
    free(inv);
    /* Atelier: palindrome*/
    printf("%d\n", palindrome(strSc));

    /* conversion chaine -> nombre*/
    /* vers un int */
    printf("%d\n", atoi("123"));        /* 123 */
    printf("%d\n", atoi("456aze"));     /* 456 */
    printf("%d\n", atoi("Bonjour"));    /* 0 */

    /* long */
    printf("%ld\n", atol("5678"));  /* 5678 */

    /* double */
    printf("%lf\n", atof("1.23"));      /* 1.23 */
    printf("%lf\n", atof("1.23e3"));    /* 1230 */

    /* ctype.h -> Vérification si un caractère est */
    printf("%d\n", isalnum('a'));   /* Alpha numérique*/
    printf("%d\n", isalnum('6'));
    printf("%d\n", isalnum('+'));

    printf("%d\n", isalpha('a'));   /* Alaphbétique */
    printf("%d\n", isalpha('3'));

    printf("%d\n", isdigit('a'));   /* Un nombre*/
    printf("%d\n", isdigit('5'));

    printf("%d\n", isspace('\t'));  /* un caractère balnc */
    printf("%d\n", isspace(' '));
    printf("%d\n", isspace('_'));

    printf("%d\n", isupper('a'));   /* une majuscule*/
    printf("%d\n", isupper('Z'));
    printf("%c\n", tolower('Z'));
    printf("%c\n", tolower('e'));   /* une minuscule */

    /* math.h -> Fonction mathématique*/
    printf("%lf\n", pow(3.0, 2.0)); /* puissance : 3² */
    printf("%lf\n", sqrt(9.0));     /* racine carré : 3 */
    printf("%lf\n", cos(M_PI / 2)); 
    printf("%lf\n", sin(M_PI / 2));

    printf("%lf\n", trunc(1.4)); /* 1.0*/
    printf("%lf\n", round(1.4)); /* 1.0 */
    printf("%lf\n", round(1.6)); /* 2.0 */
    printf("%lf\n", ceil(2.2));  /*3.0*/
    printf("%lf\n", ceil(2.6));  /*3.0*/
    printf("%lf\n", floor(2.6)); /*2.0*/
    printf("%lf\n", floor(2.2)); /*2.0*/

    printf("RAND_MAX= %d",RAND_MAX);
    printf("%d\n", rand());     /* valeur aléatoire entre 0 et RAND_MAX.*/
    return 0;
}