#ifndef COMPTE_BANCAIRE
#define COMPTE_BANCAIRE

typedef struct
{
    double solde;
    char iban[10];
    char titulaire[30];
} compteBancaire;

void afficherCompte(const compteBancaire *compte);
void crediter(compteBancaire *compte, double valeur);
void debiter(compteBancaire *compte, double valeur);
int estPositif(const compteBancaire *compte);
#endif
