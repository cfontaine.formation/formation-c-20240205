#ifndef FICHIER_H
#define FICHIER_H

#include "compteBancaire.h"

void ecrireTexte(char *);
void lireTexte(char *);
void ecritureTexteFormat(char *path);
void lectureTexteFormat(char *path);
void ecritureBin(char *path, compteBancaire cb[], int size);
void lectureBin(char *path, compteBancaire cb[], int size);
void testDeplacement(char *path);
#endif