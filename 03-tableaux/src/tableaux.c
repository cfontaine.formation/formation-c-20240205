#include <stdio.h>
/*#include <limits.h>*/

int main(void)
{
    /* Tableau à une dimension*/

    /* Déclaration */
    /* double td[5]; */

    /* Initialisation des éléments du tableau */
    /*  td[0] = 0.0;
        td[1] = 0.0;
        td[2] = 0.0;
        td[3] = 0.0;
        td[4] = 0.0; */

    /* OU */
    int i = 0;
    /*  for (i = 0; i < 5; i++)
     {
         td[i] = 0.0;
     } */

    /* OU */
    double td[5] = {0.0};

    /* Accès à un élémént */
    td[1] = 3.14;
    printf("%tf\n", td[1]);

    /* erreur : accès en dehors du tableau
    /!\ Il n'y pas de verification avec le C */
    printf("%lf\n", td[40]);

    printf("%d\n", sizeof(td)); /* -> taille du tableau 40 octet*/
    /* Calculer le nombre d'élément d'un tableau */
    printf("%d\n", sizeof(td) / sizeof(td[0])); /*5*/

    /* Parcourir un tableau à une dimension */
    for (i = 0; i < 5; i++)
    {
        printf("td[%d]=%lf\n", i, td[i]);
    }

    /* Déclaration et initialisation */
    int ti[] = {3, 6, -4, 7, 10}; /* Taille du tableau -> le nombre de valeur */
    for (i = 0; i < 5; i++)
    {
        printf("ti[%d]=%d\n", i, ti[i]);
    }

    int ti2[5] = {3, 6, -4, 7, 10};
    for (i = 0; i < 5; i++)
    {
        printf("ti2[%d]=%d\n", i, ti2[i]);
    }

    /* Les 3 valeurs qui ne sont pas définie sont égale à 0
       3,6,0,0,0 */
    int ti3[5] = {3, 6};
    for (i = 0; i < 5; i++)
    {
        printf("ti3[%d]=%d\n", i, ti3[i]);
    }

    /* Tous les éléments du tableau sont initialisés à 0 */
    int ti4[7] = {0};
    for (i = 0; i < 4; i++)
    {
        printf("ti3[%d]=%d\n", i, ti4[i]);
    }

    /* Atelier : tableau
       Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3 */
    int tab[] = {-7, -4, -8, -10, -3};
    int max = tab[0]; /*INT_MIN*/
    double somme = 0.0;
    for (i = 0; i < 5; i++)
    {
        if (tab[i] > max)
        {
            max = tab[i];
        }
        somme += tab[i];
    }
    printf("Maximum=%d Moyenne=%lf", max, somme / 5);

    /* tableau à 2 dimensions*/
    /* Déclaration */
    int tab2d[3][4];

    /* Initialisation */
    int j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            tab2d[i][j] = 0;
        }
    }
    /* OU */
    int tab2d[3][4] = {0};

    /* Accès à un élément */
    tab2d[1][1] = 65;
    printf("%d\n", tab2d[1][1]);

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            printf("%d\t", tab2d[i][j]);
        }
        printf("\n");
    }

    /* Pour les tableaux à n dimension ont peut entourer les lignes avec des accolades*/
    /* char t2d[3][2]={{'A','Z'},{'E','R'},{'T','Y'}}; */

    /* char t2d[3][2]={'A','Z','E','R','T','Y'}; */
    /* les valeurs d'initialisations qui ne sont pas définie sont initialisé à 0  */
    char t2d[3][2] = {'A', 'Z', 'E'};
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            printf("%c\t", t2d[i][j]);
        }
        printf("\n");
    }

    printf("%d\n", sizeof(t2d[1]));

    /* Déclaration d'un tableau à 3 dimmensions*/
    int tab3d[3][4][2];
    return 0;
}