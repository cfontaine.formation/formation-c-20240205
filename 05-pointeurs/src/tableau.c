#include "tableau.h"
#include <stdio.h>
#include <stdlib.h>

int *saisirTab(int *size)
{
    printf("Nombre d element du tableau ");
    scanf("%d", size);
    if (*size > 0)
    {
        int *tab = malloc(*size * sizeof(int));
        if (tab != NULL)
        {
            int i;
            for (i = 0; i < *size; i++)
            {
                printf("t[%d]=", i);
                scanf("%d", &tab[i]);
            }
            return tab;
        }
    }
    else
    {
        printf("Erreur: le nombre d'élément doit être supérieur à 0");
    }
    return NULL;
}

void afficherTab(int tab[], int size)
{
    printf("[ ");
    int i;
    for (i = 0; i < size; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("]\n");
}

int *redimTab(int tab[], int oldSize, int *newSize)
{
    printf("Nombre d element du tableau ");
    scanf("%d", newSize);
    if (*newSize > 0)
    {
        int *tmp = realloc(tab, *newSize * sizeof(int));
        if (tmp != NULL && *newSize > oldSize)
        {
            int i;
            for (i = oldSize; i < *newSize; i++)
            {
                tmp[i] = 0;
            }
        }
        return tmp;
    }
    printf("Erreur: le nombre d element doit etre superieur a 0\n");
    *newSize = oldSize;
    return tab;
}

void calculTab(int tab[], int size, double *moyenne, int *maximum)
{
    *maximum = tab[0];
    double somme = 0.0;
    int i;
    for (i = 0; i < size; i++)
    {
        if (tab[i] > *maximum)
        {
            *maximum = tab[i];
        }
        somme += tab[i];
    }
    *moyenne = somme / size;
}
