#include <stdio.h>
#include <stdlib.h>
#include "exemple.h"
#include "menu.h"

int main(void)
{
    /* Rappel pointeur*/
    int x = 10;
    printf("x=%d (adresse de x:%p)\n", x, &x);

    /* Declaration du pointeur*/
    int *ptrX = &x;
    printf("ptrX=%p\n", ptrX);

    /* Contenu de la variable pointée */
    printf("*ptrX=%d\n", *ptrX);
    *ptrX = 40;
    printf("x=%d\n", x);

    /* Pointeur constant */
    /* En préfixant avec const un pointeur*/
    const int *constPtrX = &x;
    printf("%d\n", *constPtrX);
    /* On ne peut plus modifier le contenu pointer => accès uniquement en lecture*/
    /* *constPtrX=60; */
    constPtrX = NULL;

    /* En postfixant avec const un pointeur => pointeur devient constant */
    int *const ptrConstX = &x;
    printf("%d\n", *ptrConstX);
    *ptrConstX = 80;
    /*ptrConstX=NULL;*/

    const int *const ptrConst = &x;
    printf("%d\n", *ptrConst);
    /*  *ptrConst=100;
      ptrConst=NULL;*/

    /* Affectation de pointeur */
    /* On peut affecter des pointeur du même type ou avec NULL */
    int *ptr1 = &x;
    int *ptr2;
    ptr2 = ptr1;
    printf("ptr1=%p ptr2=%p\n", ptr1, ptr2);
    printf("*ptr1=%d *ptr2=%d\n", *ptr1, *ptr2);
    ptr1 = NULL;
    printf("ptr1=%p ptr2=%p\n", ptr1, ptr2);
    printf("*ptr2=%d\n", *ptr2);

    /*Affecter des Pointeurs de type différent -> (cast)*/
    int v = 0x4F20F621;
    printf("%x %d\n", v, v);
    int *ptrV = &v;
    printf("ptrv=%p\n", ptrV);
    char *ptrC = (char *)ptrV;
    printf("ptrC= %p %x\n", ptrC, *ptrC);

    /* Arthimétque des pointeurs */
    /* Addition,
     - on peut ajouter une valeur entière à un pointeur
     - on peut incrémenter le pointeur */
    printf("ptrC= %p %x\n", (ptrC + 1), *(ptrC + 1)); /* F6 */
    printf("ptrC= %p %x\n", (ptrC + 2), *(ptrC + 2)); /* 20 */
    printf("ptrC= %p %x\n", (ptrC + 3), *(ptrC + 3)); /* 4F */
    ptrC++;
    printf("ptrC= %p %x\n", ptrC, *ptrC);

    /* Soustraction */
    printf("ptrC= %p %x\n", (ptrC - 1), *(ptrC - 1));
    ptrC--;
    printf("ptrC= %p %x\n", ptrC, *ptrC);
    char *ptrFin = ptrC + 2;
    printf("distance entre les pointeurs=%d", ptrFin - ptrC);

    /* Comparaison */
    double d1 = 4.5;
    double d2 = 8.9;

    double *p1 = &d1;
    double *p2 = &d1;
    double *p3 = &d2;

    if (p1 == p2)
    {
        printf("p1 = p2\n");
    }
    if (p1 == p3)
    {
        printf("p1 = p3\n");
    }
    else
    {
        printf("p1 != p3\n");
    }

    p2 = NULL;
    if (p2 == NULL)
    {
        printf("p2 est NULL\n");
    }
    if (!p2)
    {
        printf("p2 est NULL\n");
    }

    /* pointeur et tableau */
    int t1[] = {3, 7, 4, 8, 3, 10, 6};

    /* t1 équivaut à un pointeur sur le premier */
    printf("%p %d\n", t1, *t1);
    int *ptr = t1;
    printf("%p %p\n", ptr, t1);
    printf("%p %d\n", ptr + 1, *(ptr + 1));
    printf("%p %d\n", t1 + 1, *(t1 + 1));
    printf("%d %d\n", t1[1], ptr[1]);
    ptr++;
    printf("%p %d\n", ptr, *ptr);
    printf("%d\n", ptr[-1]);

    /* pointeur et tableau à 2 dimensions */
    int tab2d[3][2] = {{4, 6}, {7, 9}, {3, 2}};

    printf("%p %p \n", tab2d, (int *)tab2d);
    printf("%p %d %d\n", tab2d + 1, tab2d[0][1], *((int *)tab2d + 1));
    printf("%p %d %d\n", tab2d + 2, tab2d[1][0], *((int *)tab2d + 2));

    /* Passage en paramètre de tableau */
    testParamTab(t1, 7);
    testParamTabPtr(t1, 7);

    /* Pointeur générique void *  */
    double d = 1.23;
    double *ptrD = &d;
    printf("%p %lf\n", ptrD, *ptrD);
    void *ptrGen = ptrD;
    /* un pointeur générique ne permet pas  : */
    /* - Le déférencement */
    /* printf("%p\n", ptrGen); */
    /* - l’arithmétique de pointeur */
    /* ptrGen++; */

    double *ptrD2 = ptrGen;
    printf("%p %lf\n", ptrD2, *ptrD2);

    /* Allocation dynamique de mémoire */
    double *ptrA = malloc(sizeof(double));
    /* malloc -> la mémoire alloué n'est pas initialisé */
    if (ptrA != NULL) /* On vérifie que malloc à bien fonctionné*/
    {
        printf("%p\n", ptrA);
        *ptrA = 23.45;
        printf("%lf\n", *ptrA);
        /* Libération de la mémoire*/
        free(ptrA);
        printf("%p\n", ptrA);
        ptrA = NULL;
        printf("%p\n", ptrA);
        /* Si le pointeur passé à free est null, il n'y aura pas d'erreur et free ne fera rien */
        free(ptrA);
    }

    /* allocation dynamique de tableau*/
    int st = 6;
    int *tabDyn = malloc(sizeof(int) * st);
    if (tabDyn) /* correspond à tabDyn!=NULL*/
    {
        int i;
        for (i = 0; i < st; i++)
        {
            tabDyn[i] = 0;
        }
        tabDyn[2] = 123;
        testParamTab(tabDyn, st);
        free(tabDyn);
        tabDyn = NULL;
    }

    /* calloc -> la mémoire alloué est initialisé à 0 */
    tabDyn = calloc(st, sizeof(int));
    if (tabDyn != NULL)
    {
        tabDyn[1] = 3;
        testParamTab(tabDyn, st);
        free(tabDyn);
        tabDyn = NULL;
    }

    /* realloc -> Changer la taille d'une zone allouée */
    char *ptrChr = malloc(20);
    if (ptrChr != NULL)
    {
        int i;
        for (i = 0; i < 20; i++)
        {
            ptrChr[i] = 'a' + i;
        }
        ptrChr = realloc(ptrChr, 15);
        if (ptrChr != NULL)
        {
            for (i = 0; i < 15; i++)
            {
                printf("%c ", ptrChr[i]);
            }
        }
        ptrChr = realloc(ptrChr, 40);
        if (ptrChr != NULL)
        {
            for (i = 15; i < 40; i++)
            {
                ptrChr[i] = 'a' + i;
            }
            for (i = 0; i < 40; i++)
            {
                printf("%c ", ptrChr[i]);
            }
        }
        free(ptrChr);
        ptrChr = NULL;
        printf("\n");

        int *tdy = testRetourTableau(8, 2);
        testParamTab(tdy, 8);
        free(tdy);

        /*    int *tdz=testRetourLocal(4,10);
            testParamTab(tdz,4);*/

        int *tda = NULL;
        printf("adresse tda=%p\n", &tda);
        int res = testRetourTableauParam(&tda, 4, 1);
        printf("tda=%p\n", tda);
        free(tda);

        /* Pointeur de fonction */
        double (*ptrFonction)(double, double);

        printf("Choix=");
        int choix;
        scanf("%d", &choix);
        if (choix == 1)
        {
            ptrFonction = addition;
        }
        else
        {
            ptrFonction = multiplier;
        }
        printf("%p\n", ptrFonction);

        double pfa = (*ptrFonction)(4.5, 3.5);
        printf("%lf\n", pfa);

        double pfb = ptrFonction(4.5, 3.5);
        printf("%lf\n", pfb);

        /* Passage d'une fonction en paramètre */
        /* On peut passer en paramètre à la fonction calcul une fonction qui correspond 
           au pointeur -> retourne un double et à 2 double en paramètre */
        calcul(3.0, 2.0, addition);
        calcul(3.4, 2.0, multiplier);

        /* Atelier : Tableau dynamique*/
        menu();
        return 0;
    }
}