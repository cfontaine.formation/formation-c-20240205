#ifndef TABLEAU_H
#define TABLEAU_H
int *saisirTab(int *size);
void afficherTab(int tab[], int size);
int *redimTab(int tab[], int oldSize, int *newSize);
void calculTab(int tab[], int size, double *moyenne, int *maximum);
#endif
