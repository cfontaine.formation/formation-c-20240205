#ifndef EXEMPLE_H
#define EXEMPLE_H

/* Passage d'un tableau en paramètre */
void testParamTab(int t[], int size);
void testParamTabPtr(int *t, int size);

int *testRetourTableau(int size, int initVal);
/* int* testRetourLocal(int size,int initVal); */
int testRetourTableauParam(int **tab, int size, int initVal);

/* Pointeur de fonction*/
double addition(double a, double b);
double multiplier(double a, double b);
void calcul(double v1, double v2, double (*ptrF)(double, double));
#endif