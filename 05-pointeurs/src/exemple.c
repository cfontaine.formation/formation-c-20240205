#include "exemple.h"
#include <stdio.h>
#include <stdlib.h>

void testParamTab(int t[], int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        printf("%d\t", t[i]);
    }
    printf("\n");
}
// t     |
//      {3, 7, 4, 8, 3, 10, 6};
// ptr                         |
void testParamTabPtr(int *t, int size)
{
    /*     int i;
        for(i=0;i<size;i++){
            printf("%d\t",t[i]); // t[i] correspont *(t+i)
        }
        printf("\n"); */

    int *ptr;
    for (ptr = t; ptr < (t + size); ptr++)
    {
        printf("%d\t", *ptr);
    }
    printf("\n");
}

int *testRetourTableau(int size, int initVal)
{
    int *tmp = malloc(sizeof(int) * size);
    if (tmp != NULL)
    {
        for (int i = 0; i < size; i++)
        {
            tmp[i] = initVal;
        }
    }
    return tmp;
}

/* ici on retourne un pointeur sur un tableau local
   il est supprimé lorsque l'on quitte la fonction (fermeture du bloc) 
   on retourne un pointeur sur un tableau qui n'existe plus
*/
/*
int *testRetourLocal(int size, int initVal)
{
    int tmp[8];
    for (int i = 0; i < size; i++)
    {
        tmp[i] = initVal;
    }
    return tmp;
}*/

/* On utilise un paramètre pour passer en sortie le tableau, on utilise un pointeur de pointeur*/
int testRetourTableauParam(int **tab, int size, int initVal)
{
    *tab = malloc(sizeof(int) * size);
    printf("%p\n", *tab);
    if (*tab != NULL)
    {
        for (int i = 0; i < size; i++)
        {
            *((*tab) + i) = initVal; /* (*tab)[i]  */
        }
    }
    printf("%p\n", *tab);
    return *tab != NULL;
}

double addition(double a, double b)
{
    return a + b;
}
double multiplier(double a, double b)
{
    return a * b;
}


void calcul(double v1, double v2, double (*ptrF)(double, double))
{
    double tmp = ptrF(v1, v2);
    printf("%lf\n", tmp);
}
