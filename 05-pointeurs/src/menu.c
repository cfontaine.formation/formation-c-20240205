#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "tableau.h"

void afficherMenu()
{
    printf("1 - saisir un tableau\n");
    printf("2 - afficher un tableau\n");
    printf("3 - redimensionner le tableau\n");
    printf("4 - afficher le maximum et la moyenne du tableau\n");
    printf("0 - quitter\n");
    printf("choix=");
}

int saisirMenu()
{
    int choix;
    for (;;)
    {
        fflush(stdin);
        scanf("%d", &choix);
        if (choix < 0 | choix > 4)
        {
            printf("Choix: %d non valide\n", choix);
        }
        else
        {
            printf("\n");
            return choix;
        }
    }
}

void menu()
{
    int *tab = NULL;
    int size = 0;
    int choix = 0;
    do
    {
        afficherMenu();
        choix = saisirMenu();
        switch (choix)
        {
        case 1:
            if (tab != NULL)
            {
                free(tab);
            }
            tab = saisirTab(&size);
            break;
        case 2:
            if (tab)
            {
                afficherTab(tab, size);
            }
            break;
        case 3:
            if (tab)
            {
                tab=redimTab(tab, size, &size);
            }
            break;
        case 4:
            if (tab)
            {
                int maximum;
                double moyenne;
                calculTab(tab, size, &moyenne, &maximum);
                printf("maximum=%d moyenne=%lf\n", maximum, moyenne);
            }
            break;
        }
    } while (choix != 0);
    free(tab);
}
