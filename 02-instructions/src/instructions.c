#include <stdio.h>

#define SAMEDI 6
#define EPS 0.000000000000000000001

int main(void)
{
    /* condition if */
    int val;
    scanf("%d", &val);
    if (!val) /* équivalent à val == 0 */
    {
        printf("val == 0\n");
    }
    if (val > 10)
    {
        printf("val >10\n");
    }
    else if (val == 10)
    {
        printf("val = 10\n");
    }
    else
    {
        printf("val < 10\n");
    }

    /* Atelier Trie de 2 Valeurs
       Saisir 2 nombres à virgule flottante et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5 */
    double v1, v2;
    scanf("%lf %lf", &v1, &v2);
    if (v1 < v2)
    {
        printf("%lf < %lf", v1, v2);
    }
    else
    {
        printf("%lf <= %lf", v2, v1);
    }

    /* Atelier Intervalle
       Saisir un nombre entier et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus) */
    int v;
    scanf("%d", &v);
    if (v > -4 && v <= 7)
    {
        printf("%d fait parti de l'interval", v);
    }
    /* vider le buffer de scanf
       char ch=' ';
       while(ch!='\n' && ch!=EOF){
        ch=getchar();
       }*/

    /* Opérateur ternaire */
    double valeur;
    scanf("%lf", &valeur);
    double vabs = valeur < 0.0 ? -valeur : valeur;
    printf("|%lf|=%lf", valeur, vabs);

    /* Condition switch */
    int jours;
    scanf("%d", &jours);
    switch (jours)
    {
    case 1:
        printf("Lundi\n");
        break;  /* s'il n'y a pas de break le code continura à s'executer jusqu'a
                   la fin du switch ou jusqu'au prochain break */
    case SAMEDI:
    case SAMEDI + 1:
        printf("Week end !\n");
        break;
    default:
        printf("Un autre jour\n");
    }

    /*  Atelier Calculatrice
        Faire un programme calculatrice
        Saisir dans la console
        - un double
        - une caractère opérateur qui a pour valeur valide : + - * /
        - un double

        Afficher:
        - Le résultat de l’opération
        - Une message d’erreur si l’opérateur est incorrecte
        - Une message d’erreur si l’on fait une division par 0
    */
    double va, vb;
    char op;
    scanf("%lf %c %lf", &va, &op, &vb);
    switch (op)
    {
    case '+':
        printf("%lf + %lf = %lf\n", va, vb, va + vb);
        break;
    case '-':
        printf("%lf - %lf = %lf\n", va, vb, va - vb);
        break;
    case '*':
        printf("%lf * %lf = %lf\n", va, vb, va * vb);
        break;
    case '/':
        /*  pour tester l'égalité ou la différence de nombre réel provenant d'un calcul */
        /* if(vb>-EPS && vb<EPS) */
        if (vb != 0.0) 
        {
            printf("%lf / %lf = %lf\n", va, vb, va / vb);
        }
        else
        {
            printf("Division par 0\n");
        }

        break;
    default:
        printf("%c n'est pas un operateur correct\n", op);
    }

    /* Boucle: while */
    int j = 0;
    while (j < 10)
    {
        printf("%d\n", j);
        j++;
    }

    /* Boucle: do while */
    j = 0;
    do
    {
        printf("%d\n", j);
        j++;
    } while (j < 10);

    /* Boucle: for */
    for (j = 0; j < 10; j++)
    {
        printf("%d\n", j);
    }

    /* Instructions de branchement */
    /* break */

    for (j = 0; j < 10; j++)
    {
        if (j == 3)
        {
            break; /* break -> on quitte la boucle */
        }
        printf("%d\n", j);
    }

    /* continue */
    for (j = 0; j < 10; j++)
    {
        if (j == 3)
        {
            continue; /* continue -> on passe à l'ittération suivante */
        }
        printf("%d\n", j);
    }

    /* goto */
    /* unique utilisation de goto -> sortir de boucle imbriquées */
    int i;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            if (i == 3)
            {
                goto EXIT_LOOP;
            }
            printf("%d %d\n", i, j);
        }
    }
EXIT_LOOP:

    /*
        Atelier: Table de multiplication
        Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
        1 X 4 = 4
        2 X 4 = 8
        …
        9 x 4 = 36
        Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
    */
    int k;
    int m;
    for (;;)
    {
        scanf("%d", &m);
        if (m < 1 || m > 9)
        {
            break;
        }
        for (k = 1; k < 10; k++)
        {
            printf("%d x %d = %d\n", k, m, k * m);
        }
    }

    /*
        Atelier Quadrillage
        Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
    */
    int row, col, r, c;
    scanf("%d %d", &col, &row);
    for (r = 0; r < row; r++)
    {
        for (c = 0; c < col; c++)
        {
            printf("[ ]");
        }
        printf("\n");
    }
    return 0;
}