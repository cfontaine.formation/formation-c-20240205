#include <stdio.h> /* inclure le fichier d'en-tête stdio.h qui contient les entrée/sortie standard */

#define NOMBRE_HEURE 7 /* définir une constante en utilisant la directive de préprocesseur #define */

/* Fonction main => point d'entré du programme */
int main(void)
{
    /*
        Commentaire
        sur
        plusieurs ligne
    */

    printf("Test"); // commentaire fin de ligne uniquement à partir C11

    /* Déclaration d'une variable */
    int i;

    /* Initialisation d'une variable */
    i = 10;

    /* Déclaration multiple de variable */
    double largeur, hauteur;

    /* Déclaration et initialisation  d'une variable */
    double md = 12.3;

    /* Déclaration et intialisation multiple de variable */
    double mdd = 0, mee = 2;

    /* Littéral entier -> par défaut de type int */
    long l = 1000L;         /* Littéral long -> L */
    unsigned int ui = 345U; /* Littéral long -> U */

    /* Littéral entier -> changement de base */
    int dec = 42;   /* base par défaut: décimal base 10 */
    int hex = 0xF4; /* base 16 -> 0x */
    int oct = 043;  /* base 8 -> 0 */
    printf("décimal=%d hexadecimal=%x octal=%o\n", dec, hex, oct);

    /* Littéral virgule flotante */
    double d = 456.7;
    double d_exp = 4.567e2; /* écriture exponentiel */
    printf("%f %f\n", d, d_exp);

    /* Littéral virgule flottante => par défaut de type double */
    float f = 3.45F; /* Litgéral float -> F*/

    /* Littéral caractère */
    char chr = 'b';        /* Littéral caractère entre simple quote '' */
    char chrOctal = '\51'; /* Littéral caractère en octal -> '\ ' */
    char chrHex = '\x4d';  /* Littéral caractère en hexadécimal -> '\x ' */
    printf(" %c %c %c", chr, chrOctal, chrHex);

    /* printf -> affichage dans la console
       format -> "i=%d le caractere=%c\n"
       %d -> correspont une variable entière
       %c -> correspont une variable caractere
       \n -> au retour à la ligne */
    printf("caractere=%c  nombre=%08.2f", chrOctal, d);
    printf("decimal=%ld  hexa=%x", hex, hex);
    printf("decimal=%05d  hexa=%x", hex, hex);
    printf("decimal=%-5d  hexa=%x", hex, hex);

    /* scanf -> saisie sur l'entrée standard (clavier)
       format "%d" -> on saisie un entier
       la valeur saisie sera placé dans la variable se */
    int se;
    scanf("%d", &se);
    printf("%d", se);

    /* Saisir un seul caractère */ 
    char c = getchar();

    /* Afficher un seul caractère */
    putchar(c);

    /* format "%lf %c"
       % lf -> on saisie un double   la valeur saisie sera placé dans la variable largeur
       %c ->  on saisie un caractère la valeur saisie sera placé dans la variable chr  */
    scanf("%lf %c", &largeur, &chr);
    printf("largeur=%f chr=%c", largeur, chr);

    /* Constante */
    /* Constante en utilisant #define */
    int nbHeure = NOMBRE_HEURE;
    printf("%d\n", NOMBRE_HEURE);

    /* Constante en utilisant const */
    const int CST = 123; /* On est obligé d'initialiser une constante à l'initialisation*/
    printf("%d\n", CST);
    /*CST=1;  une fois la constante définit, on ne peut plus changer sa valeur */

    /* Convertion implicite => rang inférieur vers un rang supérieur (pas de perte de donnée)
       convertion d'un entier vers un double*/
    int ci1 = 34;
    double ci2;
    ci2 = ci1;
    printf("%d %f\n", ci1, ci2);

    /* Convertion d'un carctère vers un entier */
    char ci3 = 'a';
    int ci4 = ci3; /* Dans la variable entière va être stocker la valeur numérique 97 (code ASCII) qui correspond au caractère a */
    printf("%c %d %d\n", ci3, ci3, ci4);
    /* Dans le format "%c %d %d\n"
      pour afficher le carctère contenu dans le  char, on utilise %c
      pour afficher la valeur numérique d'un char, on utilise %d */

    /* conversion explicite => opérateur de cast (type) */
    int ce1 = 11;
    double cres1 = ce1 / 2;           /* 5 */
    double cres2 = ((double)ce1) / 2; /*5.5*/
    printf("%f %f\n", cres1, cres2);

    /* opérateur d'affection (=) fait toujours la conversion(implicite et explicite) */
    ce1 = cres2; /* 5 */
    printf("%f %d\n", cres2, ce1);

    /* Dépassement de capacité*/
    int dep = 300;         /* 0001 0010 1100	300 => 300 > 127, la valeur maximal d'un char*/
    signed char cep = dep; /*      0010 1100	44 => dans cep, on n'a que le premier octet de dep */
    printf("%d %d\n", dep, cep);

    /* Opérateur */
    /*Opérateur arithméthique */
    int op1 = 11;
    int op2 = 31;
    int res1 = op1 + op2;
    int res2 = op1 % 2; /*1*/

    printf("%d %d\n", res1, res2);

    /* Opérateur d'incrémentation */
    /*pré-incrémentation*/
    int inc = 0;
    res1 = ++inc; /* équivalant à inc =1 res1= 1 */
    printf("%d %d\n", inc, res1);

    /*post-incrémentation*/
    inc = 0;
    res1 = inc++; /* res1=0 inc=1*/
    printf("%d %d\n", inc, res1);

    /* Affectation composée */
    inc = 5;
    inc += 10;               /* correspond à inc=inc+10 */
    printf("inc=%d\n", inc); /* inc=15 */

    /* Opérateur de comparaison */
    inc = 3;
    printf("%d\n", inc == 4); /* faux -> correspont à 0 */
    res1 = inc == 3;          /* vrai -> valeur différente de 0 */
    printf("%d\n", res1);

    /* Opéreteur non ! */
    res1 = !(inc == 4); /* vrai */
    printf("%d\n", res1);

    /* Opérateur et && et ou  || */
    /*                ET       OU
         a      b     a && b   a || b
         Faux  Faux    Faux     Faux
         Vrai  Faux    Faux     Vrai
         Faux  Vrai    Faux     Vrai
         Vrai  Vrai    Vrai     Vrai

      Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
      && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
      || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
   */

    res1 = inc > 30 && inc++ == 3;               /* faux */
    printf("ET= res1=%d   inc=%d\n", res1, inc); /* 0 3*/

    res1 = inc == 3 || inc++ > 30;               /* vrai */
    printf("OU= res1=%d   inc=%d\n", res1, inc); /* 0 3*/

    /* Opérateur binaire */
    /* Pour les opérations sur des binaires, on peut utiliser les entiers non signé */
    unsigned int bin = 0x13; /* 13 en hexadecimal -> 00010011 */

    /* Complément ~ */
    printf("%x\n", ~bin); /* 11111111 11111111 11111111 11101100*/

    /* et & */
    printf("%x\n", bin & 0x2); /*  00010011
                                 & 00000010 = 10  0X2*/
                               /* ou |*/
    printf("%x\n", bin | 0x4); /*  00010011
                                 | 00000100 =  10111   0x17*/

    /* OU exclusif
    a      b     a ^ b
    Faux  Faux   Faux
    Vrai  Faux   Vrai
    Faux  Vrai   Vrai
    Vrai  Vrai   Faux
    */

    /* ou exclusif ^ */
    printf("%x", bin ^ 0x6); /*  00010011
                               ^ 00000110 =  10101   0x15*/

    /* Opérateur de décalage*/
    printf("%x\n", bin >> 1); /* 0001001      0x9     division par 2 */
    printf("%x\n", bin >> 3); /* 00010        0X2*/
    printf("%x\n", bin << 1); /* 00010 0110   0X26    multiplication par 2 */
    printf("%x\n", bin << 4); /* 000100110000 0x130*/

  
     /* Opérateur sizeof: Renvoie la taille en octet d’une variable ou d’un type  */
    printf("%d %d\n", sizeof(inc),sizeof(double));

    /* Opérateur séquentiel */
    inc = 3;
    int a=12;
    int b=3;
    int seq= (inc++,a+b); /* inc++ est bien executé mais la valeur affecté à seq et a+b*/
    printf("seq=%d inc%d\n",seq,inc);

    /* Ajustement de type*/
    int pn1=11;
    double pn2=4.5;
    double pres= pn1+pn2;  /* pn1 va être convertie en double avant de faire l'addition 11.0 + 4.5 =>15.5 */
    double pres1=pn1/2;    /* 11/2   =>   5*/
    double pres2=pn1/2.0;  /* 11.0/2.0    =>5.5*/

   /* Promotion numérique char et short -> int */
   char c1=11;
   char c2=20;
   int pres3=c1 + c2; /* c1 et c2 vont être converti vers un entier avant d'être ajouté */

   /* Moyenne 
      Saisir 2 nombres entiers et afficher la moyenne dans la console */
   int v1,v2;
   printf("Saisir 2 entiers");
   scanf("%d %d",&v1,&v2);
   double moyenne=(v1+v2)/2.0;
   /* OU double moyenne=((double)(v1+v2)) /2; */
   printf("moyenne=%f",moyenne);

   /* Pointeur */
   double valeur=3.14;

   /* &valeur => adresse de la variable valeur */
   printf("adresse de la variable valeur= %p\n",&valeur);

   /* Déclaration d'un pointeur de double */
   double *ptr = &valeur;
   
   /* Un pointeur contient une adresse mémoire, ici l'adresse de la variable valeur */
   printf("contenu du pointeur= %p\n",ptr);

   /* *ptr => Accès au contenu de la variable pointée par ptr */
   printf("contenu pointer par le  pointeur= %f\n",*ptr);

   *ptr=4.5;   /* modification de la variable valeur par l'intermédiaire du pointeur ptr */
   printf("valeur=%f",valeur);

    double *ptr2 = NULL; /* NULL -> Un pointeur qui ne pointe sur rien */
    /* NULL est définie dans stdio.h */
    return 0;
}