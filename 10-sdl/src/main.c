#include <SDL2/SDL.h>

int main(int argc, char *argv[])
{
  /* Initialisation de  SDL
   - SDL_INIT_VIDEO: affichage graphique et événement, SDL_INIT_TIMER, SDL_INIT_AUDIO, SDL_INIT_JOYSTICK ... */
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    return EXIT_FAILURE;
  }

  /*  Créer la fenêtre
      - titre: titre de la fenêtre
      - xp, yp: position de la fenêtre
      - w, h: largeur et largeur de la fenêtre
      - flag: SDL_WINDOW_FULLSCREEN: fenêtre pleine écran, SDL_WINDOW_RESIZABLE: fenêtre redmensionnable
              SDL_WINDOW_MINIMIZED:  fenêtre minimisable, SDL_WINDOW_MAXIMIZED: maximisable
              , SDL_WINDOW_SHOWN: fenêtre visible, SDL_WINDOW_HIDDEN, SDL_WINDOW_BORDERLESS, SDL_WINDOW_OPENGL, SDL_WINDOW_VULKAN ...
  */
  SDL_Window *pWindow = SDL_CreateWindow("Test SDL", 200, 200, 600, 400, SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);
  if (pWindow == NULL)
  {
    SDL_Quit(); /* Nettoyez tous les sous-systèmes initialisés*/
    return EXIT_FAILURE;
  }
  /* Créez un contexte de rendu 2D pour la fenêtre
      - index= -1 -> laisse SDL choisir le bon pilote de la carte graphique
      - SDL_RENDERER_SOFTWARE: accélération logiciel, SDL_RENDERER_ACCELERATED: accélération matériel,
        SDL_RENDERER_PRESENTVSYNC, SDL_RENDER_TARGETTEXTURE
  */
  SDL_Renderer *pRenderer = SDL_CreateRenderer(pWindow, -1, SDL_RENDERER_ACCELERATED);
  if (pRenderer == NULL)
  {
    SDL_DestroyWindow(pWindow); /* Détruire la fenêtre */
    SDL_Quit();
    return EXIT_FAILURE;
  }

  /* Boucle d'événement */
  SDL_Event events; /* structure contenant un événements */
  SDL_bool run = SDL_TRUE;
  Uint8 rouge = 160;
  while (run)
  {
    /* Retirer un événement dans la queue d'événement */
    while (SDL_PollEvent(&events))
    {
      switch (events.type)
      { /* type -> indique le type d'événement*/
      case SDL_WINDOWEVENT: /* événement de la fenêtre */
        if (events.window.event == SDL_WINDOWEVENT_CLOSE) /* événement de fermeture de la fenêtre */
        {
          run = SDL_FALSE;
        }
        break;
      case SDL_KEYDOWN: /* événement touche enfoncer */
        if (events.key.keysym.scancode == SDL_SCANCODE_UP && rouge > 0)
        {
          rouge--;
        }
        else if (events.key.keysym.scancode == SDL_SCANCODE_DOWN && rouge < 255)
        {
          rouge++;
        } 
        else if (events.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
        {
          run = SDL_FALSE;
        }

        break;
        /* gestion d'autre événement  ...*/
      }
    }
    /* Définir la couleur utilisée pour les opérations de dessin R, V, B, Alpha*/
    SDL_SetRenderDrawColor(pRenderer, rouge, 0, 0, 255);
    /* Efface le contexte de rendu 2D avec la couleur du dessin*/
    SDL_RenderClear(pRenderer);
    /* Mettre à jour l'écran avec tout rendu effectué depuis l'appel précédent*/
    SDL_RenderPresent(pRenderer);
  }
  /* détruire le conte*/
  SDL_DestroyRenderer(pRenderer);
  SDL_DestroyWindow(pWindow);
  SDL_Quit();
  return EXIT_SUCCESS;
}