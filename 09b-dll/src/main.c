#include "calcul.h"
#include "stdio.h"

/* Pour utiliser la dll (ici calcul), il faut ajouter dans la commande -lcalcul et avec -L indiquer le chemin de la librairie
    Il faut que la dll soit dans le même dossier que le .exe et il faut inclure le fichier d'en-tête de la calcul.h
*/
int main(void)
{
    printf("Addition=%d\n", addition(1, 3));
    return 0;
}