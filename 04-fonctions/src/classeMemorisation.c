#include "classeMemorisation.h"
#include <stdio.h>

/*extern double varGlobal;*/
extern const double PI;
/*extern int varGlobalStatic;*/

/* Classe de mémorisation static */
int testStatic(void)
{
    /* Entre 2 exécutions is conserve sa valeur */
    static int is = 100; /* par défaut initialisé à 0 */
    printf("test static: %d\n", is);
    is++; /* is n'est visible que dans la fonction */
    return is;
}

int testRegister(void)
{
    register int i; /* indique au compIlateur de stocker la variable dans un registre*/
    int somme = 0;
    for (i = 0; i < 1000; i++)
    {
        somme += i;
    }
    return somme;
}

void testExtern(void)
{
    extern double varGlobal;
    printf("%lf\n", varGlobal);
    printf("%lf\n", PI);
    /*printf("%d\n",varGlobalStatic);*/
}
