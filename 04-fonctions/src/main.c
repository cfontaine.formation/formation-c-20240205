#include <stdio.h>
#include "mesFonctions.h"
#include "classeMemorisation.h"

/* Déclaration des fonctions => déplacer dans le fichier .h d'en-tête mesFonctions.h*/
/* double multiplier(double, double);
void afficher(double);
int maximum(int, int);
int even(int); */

/* Variable globale*/
double varGlobal;

const double PI = 3.14;

/* avec static la variable globale n'est visible que dans ce fichier */
static int varGlobalStatic;

int main(int argc, char *argv[])
{
    /* Appel de la fonction */
    double res = multiplier(3.4, 5.6);
    printf("%f\n", res);
    printf("%f\n", multiplier(1.2, 3.4));

    /* Appel de methode (sans retour) */
    afficher(res);

    /* Atelier: Fonction maximum */
    int a, b;
    scanf("%d %d", &a, &b);
    printf("Le maximum est %d", maximum(a, b));

    /* Atelier: Fonction paire */
    printf("%d\n", even(6));
    printf("%d\n", even(5));

    /* Paramètre passé par valeur (par défaut)
      C'est une copie de la valeur du paramètre qui est transmise à la fonction */
    int va = 42;
    testParamValeur(va);
    printf("%d\n", va);

    /* Paramètre passé par adresse
       La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la fonction */
    va = 42;
    testParamAdresse(&va);
    printf("%d\n", va);

    /* Atelier permuter */
    a = 10;
    b = 20;
    printf("%d %d\n", a, b);
    swap(&a, &b);
    printf("%d %d\n", a, b);

    /* Les tableaux peuvent être passé par adresse et pas par valeur */
    int i = 123;
    int tab[] = {4, 6, -7, 3, 7, 4};
    afficherTab(tab, 6);
    printf("%d \n", i);

    /* Variable locale */
    int lo = 23;
    if (lo > 0)
    {
        int loi = 5;
        lo = loi + 3;
        printf("loi=%d", loi);
    }
    printf("lo=%d", lo);

    /* Variable  globale*/
    printf("Variable globale=%lf\n", varGlobal);

    varGlobal = 34.5;
    printf("Variable globale=%lf\n", varGlobal);

    /* Masquage de variable */
    double varGlobal = 3.5; /* La variable locale va masquer la variable global*/
    printf("%lf\n", varGlobal);

    /*Classe de mémorisation
     Variable locale
     Classe de mémorisation static */
    testStatic();
    testStatic();
    int ris = testStatic();
    printf("%d\n", ris);
    testStatic();

    /* Variable globale
    Classe de mémorisation  register */
    printf("%d\n", testRegister());

    /* Classe de mémorisation  extern */
    testExtern();

    varGlobalStatic = 45;
    printf("%d\n", varGlobalStatic);

    /* Atelier: bissextile */
    printf("2023 %d\n", bissextile(2023));
    printf("1900 %d\n", bissextile(1900));
    printf("2000 %d\n", bissextile(2000));
    printf("2024 %d\n", bissextile(2024));

    /* Atelier: dernier jour du mois */
    printf("%d\n", dernierJourMois(2024, 6));
    printf("%d\n", dernierJourMois(2024, 12));
    printf("%d\n", dernierJourMois(2000, 2));
    printf("%d\n", dernierJourMois(1999, 2));

    /* Fonction récurssive */
    int fact3 = factorial(3);
    printf("3!=%d\n", fact3);

    /* Paramètre fonction main */
    for (i = 0; i < argc; i++)
    {
        printf("%s\n", argv[i]);
    }

    /* Nombre de paramètre variable */
    printf("%lf", moyenne(3, 1, 2, 3));
    printf("%lf", moyenne(0));
    printf("%lf", moyenne(2, 5, 4));

    return 0; /* -> indique au système d'explotation que la fonction c'est bien déroulée*/
}

/* Définition des fonctions -> déplacer dans le fichier mesFonctions.c*/

/* double multiplier(double d1, double d2)
{
    return d1 * d2;
}

void afficher(double d)
{
    printf("%lf\n", d);
    return;
}

 int maximum(int a, int b)
{
    return a > b ? a : b;
     OU
    if (a > b)
    {
        return a;
    }
    else
    {
        return b;
 }

int even(int v)
{
     return !(v & 1);
     OU
     return !(v % 2); équivaut v%2==0
      OU
     if(v%2==0){
         return 1;
     }
     else
     {
         return 0;
     }
} */
