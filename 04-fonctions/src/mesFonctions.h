#ifndef MES_FONCTIONS_H
#define MES_FONCTIONS_H

/* Fichier .h -> contient la déclaration des fonctions */

#define TRUE 1
#define FALSE 0

/* Déclaration d'une fonction */
double multiplier(double, double);

/* Déclaration d'une fonction sans retour -> void */
void afficher(double);

/* Atelier : fonction maximum */
int maximum(int, int);

/* Atelier : fonction paire */
int even(int);

/* Passage de paramètre par valeur */
void testParamValeur(int a);

/* Passage de paramètre par adresse
   En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre */
void testParamAdresse(int *a);

/* Atelier : permuter */
void swap(int *, int *);

/* Passage d'un tableau, comme paramètre d'une fonction
  On ne peut pas passer un tableau par valeur uniquement par adresse */
void afficherTab(int[], int);

/* Atelier: bissextile */
int bissextile(int);

/* Atelier: dernier jour du mois */
int dernierJourMois(int, int);

/* Récursivité */
int factorial(int);

/* Fonction à nombre variable de paramètre */
double moyenne(int nbParam, ...);

#endif