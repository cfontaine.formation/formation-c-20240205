#ifndef CALCUL_H
#define CALCUL_H
#ifdef EXPORT
/* Pour compiler la dll, il faut ajouter à la ligne de commande de gcc -shared et-D EXPORT et compiler en release avec -s
   Pour les fonctions, il faut ajouter entre le nom et le type de retour __declspec()
   - avec dllexport pour la compilation de la dll
   - avec dllimport pour l'utilisation de la dll
*/
    #define DLL_FUNCTION __declspec(dllexport)
#else
    #define DLL_FUNCTION __declspec(dllimport)
#endif
int DLL_FUNCTION addition(int a, int b);
int DLL_FUNCTION multiplication(int a, int b);
#endif
