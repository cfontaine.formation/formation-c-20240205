#ifndef CERCLE_H
#define CERCLE_H
#include "point.h"

struct cercle
{
    double rayon;
    struct point centre; /* struture imbriquée */
};

void afficherCercle(const struct cercle *c);
#endif