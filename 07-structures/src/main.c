#include "point.h"
#include "cercle.h"
#include "compteBancaire.h"
#include "liste.h"
#include "mesStructures.h"
#include <stdio.h>

struct point3d
{
    double x;
    double y;
    double z;
} p3d;

enum motorisation
{
    ESSENCE = 95,
    DIESEL = 4,
    GPL = 45,
    ETHANOL = 2,
    ELECTRIQUE = 1
};

double addition(double a, double b)
{
    return a + b;
}

int main(void)
{
    struct point p1, p2;
    p1.x = 0;
    p1.y = 0;

    p2.x = 1;
    p2.y = 2;

    printf("x=%d , y=%d", p1.x, p1.y);
    printf("x=%d , y=%d", p2.x, p2.y);

    struct point p3 = {1, 1};
    printf("x=%d , y=%d", p3.x, p3.y);

    /* Affectation */
    struct point p4;
    p4 = p3;
    p4.x = 10;
    printf("x=%d , y=%d", p3.x, p3.y);
    printf("x=%d , y=%d", p4.x, p4.y);

    /* Comparaison */
    if (p1.x == p2.x && p1.y == p2.y)
    {
        printf("Les 2 points sont égaux");
    }

    /* Tableau de structure */
    struct point tab[4];
    tab[0].x = 3;
    tab[0].y = 2;
    tab[1].x = 3;
    tab[1].y = 1;
    tab[2].x = 3;
    tab[2].y = 4;
    tab[3].x = 3;
    tab[3].y = 2;
    int i;
    for (i = 0; i < 4; i++)
    {
        printf("(%d,%d)\n", tab[i].x, tab[i].y);
    }

    struct point tab2[] = {{1, 4}, {3, 8}, {4, 6}};

    /* struture imbriquée */
    struct cercle c1;
    c1.rayon = 2.0;
    c1.centre.x = 2;
    c1.centre.y = 3;

    /* Structure pointeur et fonction */
    struct point a, b;
    initialisationPoint(&a, 0, 0);
    afficherPoint(&a);
    initialisationPoint(&b, 1, 1);
    afficherPoint(&b);
    printf("distance =%lf\n", distance(&a, &b));

    afficherCercle(&c1);

    /* typedef */
    tpoint r = {3, 6};
    afficherPoint(&r);

    /* avec un pointeur de fonction */
    typedef double (*operation)(double, double);

    operation op = addition;
    printf("%d\n", op(1, 3));

    /* Compte bancaire */
    compteBancaire cb = {30.0, "fr59-0000", "John Doe"};
    afficherCompte(&cb);
    crediter(&cb, 50.5);
    crediter(&cb, 600.0);
    debiter(&cb, 100.0);
    afficherCompte(&cb);
    printf("%d\n", estPositif(&cb));
    debiter(&cb, 1000.0);
    afficherCompte(&cb);
    printf("%d\n", estPositif(&cb));

    /* liste */
    ptrElementList tl = initList();
    ajouterElement(&tl, 4);
    ajouterElement(&tl, 2);
    ajouterElement(&tl, 6);
    ajouterElement(&tl, 1);
    ajouterElement(&tl, 3);
    afficherList(tl);
    effacerList(&tl);

    /* Union */
    union prime pri;
    pri.fixe = 1000;
    printf("%d\n", pri.fixe);
    pri.taux = 0.10;
    printf("%lf\n", pri.taux);
    printf("%d\n", pri.fixe);

    /*Champs de bits */
    struct word w;
    w.leds = 0x1F;
    w.bp1 = 1;
    w.bp2a6 = 0x3;

    printf("%x %x %x\n", w.bp1, w.leds, w.bp2a6);

    /* Enumération */
    enum motorisation m = GPL;
    printf("%d\n", m);
    /* enum -> int */
    int imt = m;
    printf("%d\n", imt);
    /* enum <- int  = switch */

    /* Preprocesseur*/
    /* Macro */
    printf("%d\n", ADDITION(1, 2));
    /* Compilation conditionnel */
    printf("%s\n", EXEMPLE);
    /* Constante prédéfinie*/
    printf("%s\n", __DATE__);
    return 0;
}