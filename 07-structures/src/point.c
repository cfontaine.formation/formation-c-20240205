#include "point.h"
#include <stdio.h>
#include "math.h"

void afficherPoint(const struct point *p)
{
 /*    printf("(%d,%d)",(*p).x,(*p).y); */
    printf("(%d,%d)",p->x,p->y);
}

void initialisationPoint(struct point *p, int x, int y)
{
    p->x=x;
    p->y=y;
}

double distance(const struct point *pa, const struct point *pb)
{
    return sqrt(pow(pb->x-pa->x,2.0)+pow(pb->y-pa->y,2.0));
}
