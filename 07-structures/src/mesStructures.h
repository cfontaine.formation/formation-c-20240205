#ifndef MES_STRUCTURES_H
#define MES_STRUCTURES_H

/* Macro */
#define ADDITION(a,b) (a+b)

union prime
{
    int fixe;
    double taux;
};

struct word
{
    unsigned int bp1:1;
    unsigned int :4;
    unsigned int leds:5;
    unsigned int bp2a6:5;
};

#if __WIN32__
    #define EXEMPLE "windows"
#else
    #define EXEMPLE "autreOS"
#endif

#endif // !MES_STRUCTURES_H