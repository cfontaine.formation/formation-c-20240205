#include "compteBancaire.h"
#include <stdio.h>

void afficherCompte(const compteBancaire *compte)
{
    printf("_______________________\n");
    printf("Solde = %lf\nIban=%s\nTitulaire=%s\n", compte->solde, compte->iban, compte->titulaire);
    printf("_______________________\n");
}

void crediter(compteBancaire *compte, double valeur)
{
    if (valeur > 0.0 && compte != NULL)
    {
        compte->solde += valeur;
    }
}
void debiter(compteBancaire *compte, double valeur)
{
    if (valeur > 0.0 && compte != NULL)
    {
        compte->solde -= valeur;
    }
}
int estPositif(const compteBancaire *compte)
{
    return compte->solde >= 0;
}