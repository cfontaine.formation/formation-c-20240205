#include "liste.h"
#include <stdlib.h>
#include <stdio.h>

ptrElementList initList(void)
{
    return NULL;
}
void ajouterElement(ptrElementList *list, int data)
{
    ptrElementList elm = malloc(sizeof(struct elementList));
    elm->data = data;
    elm->suivant = *list;
    *list = elm;
}
void afficherList(ptrElementList list)
{
    ptrElementList ptr = list;
    while (ptr != NULL)
    {
        printf("%d\n", ptr->data);
        ptr = ptr->suivant;
    }
}
void effacerList(ptrElementList *list)
{
    ptrElementList ptr = *list;
    ptrElementList ptrDel;
    while (ptr != NULL)
    {
        ptrDel = ptr;
        ptr = ptr->suivant;
        free(ptrDel);
    }
    *list = NULL;
}