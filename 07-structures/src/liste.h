#ifndef LISTE_H
#define LISTE_H

typedef struct elementList *ptrElementList;

struct elementList
{
    ptrElementList suivant;
    int data;
};

ptrElementList initList(void);
void ajouterElement(ptrElementList *list, int data);
void afficherList(ptrElementList list);
void effacerList(ptrElementList *list);

#endif