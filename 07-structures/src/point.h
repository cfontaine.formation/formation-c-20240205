#ifndef POINT_H
#define POINT_H

/* struct point
{
    int x;
    int y;
};

typedef struct point tpoint; */

typedef struct point
{
    int x;
    int y;
} tpoint;

void afficherPoint(const tpoint *);
void initialisationPoint(struct point*,int,int);
double distance(const struct point *, const struct point *);

#endif