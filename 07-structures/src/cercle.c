#include "cercle.h"
#include <stdio.h>

void afficherCercle(const struct cercle *c)
{
    printf("rayon=%lf centre=", c->rayon);
    afficherPoint(&(c->centre));
    printf("\n");
}